﻿using DTO.Chatroom;
using System.Threading.Tasks;

namespace SignalR
{
    public class ChatroomHub : MainHub
    {
        public override async Task OnConnected()
        {
            //聊天室專用
            if (!string.IsNullOrEmpty(Context.QueryString["CId"]))
            {
                await Groups.Add(Context.ConnectionId, Context.QueryString["CId"]);
            }

            //問題清單專用
            if (!string.IsNullOrEmpty(Context.User.Identity.Name))
            {
                await Groups.Add(Context.ConnectionId, Context.User.Identity.Name);
            }

            await base.OnConnected();
        }

        public override async Task OnDisconnected(bool stopCalled)
        {
            //聊天室專用
            if (!string.IsNullOrEmpty(Context.QueryString["CId"]))
            {
                await Groups.Remove(Context.ConnectionId, Context.QueryString["CId"]);
            }

            //問題清單專用
            if (!string.IsNullOrEmpty(Context.User.Identity.Name))
            {
                await Groups.Remove(Context.ConnectionId, Context.User.Identity.Name);
            }

            await base.OnDisconnected(stopCalled);
        }

        public void SendMessage(ChatroomData data)
        {
            Clients.Group(data.ConversationId).sendMessage(data);
        }

        public void Shake()
        {
            Clients.Group(Context.QueryString["CId"]).shake();
        }

        public void ProblemNotification()
        {
            Clients.Group(Context.User.Identity.Name).problemNotification();
        }
    }
}