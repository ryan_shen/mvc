﻿using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace SignalR
{
    public class MainHub : Hub
    {
        public override async Task OnReconnected()
        {
            await base.OnReconnected();
        }
    }
}
