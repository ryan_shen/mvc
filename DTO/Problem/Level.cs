﻿namespace DTO.Problem
{
    public static class Level
    {
        public static readonly int Signup = 1;
        public static readonly int MaskTheTitles = 2;
        public static readonly int CollapsedContent = 3;
        public static readonly int Decoder = 4;
        public static readonly int InfiniteAlerts = 5;
        public static readonly int Regex = 6;
        public static readonly int Background = 7;
        public static readonly int qolq = 8;
        public static readonly int PathTraversal = 9;
        public static readonly int Page20000 = 10;
        public static readonly int DataInBrowser = 11;
        public static readonly int Unsubmittable = 12;
        public static readonly int FakeMessage = 13;
        public static readonly int PasscodeAndRoomId = 14;
        public static readonly int XSS = 15;
        public static readonly int Misconfiguration = 16;
        public static readonly int SqlInjection = 17;
        public static readonly int StackTrace = 18;
        public static readonly int PRNG = 19;
        public static readonly int Unauthorized = 20;
        public static readonly int SqlInjectionHard = 21;
    }
}
