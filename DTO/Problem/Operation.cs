﻿using System.ComponentModel.DataAnnotations;

namespace DTO.Problem
{
    public class Operation
    {
        [Required]
        [Display(Name = "Operand1")]
        public int? N1 { get; set; }

        [Required]
        [Display(Name = "Operator")]
        public string Op { get; set; }

        [Required]
        [Display(Name = "Operand2")]
        public int? N2 { get; set; }
    }
}
