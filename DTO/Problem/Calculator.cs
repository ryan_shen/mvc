﻿using System;
using System.Collections.Generic;

namespace DTO.Problem
{
    public class Calculator
    {
        public static Dictionary<string, Func<int, int, int>> OpToFunc = new Dictionary<string, Func<int, int, int>>
        {
            { "+", (a,b) => a + b},
            { "-", (a,b) => a - b},
            { "*", (a,b) => a * b},
            { "/", (a,b) => a / b},
        };

        public static int Calculate(int n1, string op, int n2)
        {
            if (!OpToFunc.ContainsKey(op))
            {
                throw new Exception("Please follow rhe rules of the calculator.:)");
            }

            Func<int, int, int> f = OpToFunc[op];

            try
            {
                return FirstInput16(f, n1, n2);
            }
            catch (DivideByZeroException)
            {
                throw;
            }
        }

        public static int FirstInput16(Func<int, int, int> f, int n1, int n2)
        {
            try
            {
                return SecondInput70(f, n1, n2);
            }
            catch (DivideByZeroException)
            {
                throw;
            }
        }

        public static int SecondInput70(Func<int, int, int> f, int n1, int n2)
        {
            try
            {
                return ThirdInput84(f, n1, n2);
            }
            catch (DivideByZeroException)
            {
                throw;
            }
        }

        public static int ThirdInput84(Func<int, int, int> f, int n1, int n2)
        {
            try
            {
                return FourthInput85(f, n1, n2);
            }
            catch (DivideByZeroException)
            {
                throw;
            }
        }

        public static int FourthInput85(Func<int, int, int> f, int n1, int n2)
        {
            try
            {
                return f(n1, n2);
            }
            catch (DivideByZeroException ex)
            {
                throw new DivideByZeroException("Attempted to divide by zero。(◔ д◔)", ex);
            }
        }
    }
}
