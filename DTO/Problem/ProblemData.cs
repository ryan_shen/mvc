﻿using Dapper.Contrib.Extensions;

namespace DTO.Problem
{
    [Table("Problem")]
    public class ProblemData
    {
        /// <summary>
        /// 問題編號
        /// </summary>
        [ExplicitKey]
        public int ProblemId { get; set; }

        /// <summary>
        /// 題目
        /// </summary>
        [Computed]
        public string ProblemName { get; set; }
    }
}
