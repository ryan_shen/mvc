﻿using System;

namespace DTO.Problem
{
    public class Page20000Data
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }
        public DateTime Date { get; set; }
    }
}
