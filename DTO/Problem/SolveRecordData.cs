﻿using Dapper.Contrib.Extensions;
using System;

namespace DTO.Problem
{
    [Table("SolveRecord")]
    public class SolveRecordData
    {
        /// <summary>
        /// 排名
        /// </summary>
        [Computed]
        public int Ranking { get; set; }

        /// <summary>
        /// 問題編號
        /// </summary>
        [ExplicitKey]
        public int ProblemId { get; set; }

        /// <summary>
        /// 問題名稱
        /// </summary>
        [Computed]
        public string ProblemName { get; set; }

        /// <summary>
        /// 解題人員代碼
        /// </summary>
        [ExplicitKey]
        public string UserId { get; set; }

        /// <summary>
        /// 解題日期
        /// </summary>
        public DateTime? SolveDate { get; set; }
    }
}
