﻿using Dapper.Contrib.Extensions;

namespace DTO.Account
{
    [Table("Account")]
    public class AccountData
    {
        /// <summary>
        /// 帳號
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 邀請碼
        /// </summary>
        [Computed]
        public string InvitationCode { get; set; }
    }
}
