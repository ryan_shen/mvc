﻿using Dapper.Contrib.Extensions;

namespace DTO.Chatroom
{
    [Table("Conversation")]
    public class ConversationData
    {
        public string RoomId { get; set; }

        public string UserId { get; set; }

        public string ConversationId { get; set; }
    }
}
