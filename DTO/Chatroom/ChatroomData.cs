﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DTO.Chatroom
{
    [Table("Chatroom")]
    public class ChatroomData
    {
        public string RoomId { get; set; }

        public string ConversationId { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }

        public string Message { get; set; }

        public DateTime? SendDate { get; set; }
    }
}
