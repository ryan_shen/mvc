﻿using DAL.Problem;
using DTO.Problem;
using Microsoft.AspNet.SignalR;
using SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BLL.Problem
{
    public class SolveRecordBLL
    {
        private SolveRecordDAL SolveRecordDAL = new SolveRecordDAL();

        public IEnumerable<SolveRecordData> GetSelfRecords(string userId)
        {
            var allProblems = SolveRecordDAL.Select(userId);

            if (!IsSolved(userId, Level.MaskTheTitles))
            {
                foreach (var problem in allProblems)
                {
                    //將所有字元轉成 * 號
                    problem.ProblemName = Regex.Replace(problem.ProblemName, ".", "*");
                }
            }

            return allProblems;
        }

        public IEnumerable<SolveRecordData> GetRanking(int problemId, int rankCount = 10)
        {
            List<SolveRecordData> rankings = SolveRecordDAL.GetRanking(problemId, rankCount).ToList();

            //填補不足的排名數
            if (rankings.Count() < rankCount)
            {
                for (int i = rankings.Count() + 1; i <= rankCount; i++)
                {
                    rankings.Add(new SolveRecordData
                    {
                        Ranking = i
                    });
                }
            }

            return rankings;
        }

        /// <summary>
        /// 將題目註記為已完成
        /// </summary>
        /// <param name="userId">使用者資訊</param>
        /// <param name="problemId">題目名稱</param>
        public void SolveProblem(string userId, int problemId)
        {
            if (IsSolved(userId, problemId))
            {
                return;
            }

            var record = new SolveRecordData
            {
                ProblemId = problemId,
                UserId = userId,
                SolveDate = DateTime.Now
            };

            SolveRecordDAL.Insert(record);

            var context = GlobalHost.ConnectionManager.GetHubContext<ChatroomHub>();
            context.Clients.Group(userId).ProblemNotification();
        }

        /// <summary>
        /// 題目是否已完成
        /// </summary>
        /// <param name="problemId">第幾關</param>
        public bool IsSolved(string userId, int problemId)
        {
            var problem = SolveRecordDAL.Select(userId, problemId);

            return problem?.SolveDate != null;
        }
    }
}
