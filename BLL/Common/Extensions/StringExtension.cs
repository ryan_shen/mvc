﻿namespace BLL.Common.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// 判斷字串是否為空或 Null
        /// </summary>
        /// <param name="s">要判斷的字串</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// 當字串為空或 Null 時回傳指定的預設值
        /// </summary>
        /// <param name="s">要判斷的字串</param>
        /// <param name="defaultValue">預設值</param>
        /// <returns></returns>
        public static string IfNullOrEmpty(this string s, string defaultValue)
        {
            if (s.IsNullOrEmpty())
            {
                return defaultValue;
            }

            return s;
        }
    }
}
