﻿using Newtonsoft.Json;

namespace BLL.Common.Extensions
{
    public static class ObjectExtension
    {
        /// <summary>
        /// 將物件映射成指定型態
        /// </summary>
        /// <typeparam name="T">映射目標型態</typeparam>
        /// <param name="o">映射來源物件</param>
        /// <returns></returns>
        public static T ToType<T>(this object o)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(o));
        }
    }
}
