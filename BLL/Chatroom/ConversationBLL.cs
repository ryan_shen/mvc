﻿using BLL.Common.Extensions;
using DAL.Chatroom;
using DTO.Chatroom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Chatroom
{
    public class ConversationBLL
    {
        private ConversationDAL ConversationDAL = new ConversationDAL();

        public string GetConversationId(string roomId, string userId)
        {
            string conversationId = ConversationDAL.GetConversationId(roomId, userId);

            if (conversationId.IsNullOrEmpty())
            {
                conversationId = Guid.NewGuid().ToString();
                ConversationDAL.Insert(new ConversationData 
                {
                    RoomId = roomId,
                    UserId = userId,
                    ConversationId = conversationId
                });
            }

            return conversationId;
        }
    }
}
