﻿using DAL.Chatroom;
using DTO.Chatroom;
using Microsoft.AspNet.SignalR;
using SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BLL.Chatroom
{
    public class ChatroomBLL
    {
        private readonly ChatroomDAL ChatroomDAL = new ChatroomDAL();

        public IEnumerable<ChatroomData> Select(ChatroomData data)
        {
            return ChatroomDAL.Select(data)
                                    .OrderBy(i => i.SendDate);
        }

        public bool HasData(ChatroomData data)
        {
            return ChatroomDAL.Select(data).Any();
        }

        public void Insert(ChatroomData data)
        {
            ChatroomDAL.Insert(data);
        }

        /// <summary>
        /// 新增訊息到資料庫並廣播到聊天室
        /// </summary>
        /// <param name="data"></param>
        public void SendMessage(ChatroomData data)
        {
            if(data.SendDate == null)
            {
                data.SendDate = DateTime.Now;
            }

            //新增訊息到資料庫
            ChatroomDAL.Insert(data);

            //廣播該訊息給所有人
            var context = GlobalHost.ConnectionManager.GetHubContext<ChatroomHub>();
            context.Clients.Group(data.ConversationId).SendMessage(data);
        }

        /// <summary>
        /// 新增訊息到資料庫並廣播到聊天室
        /// </summary>
        /// <param name="data"></param>
        /// <param name="userId"></param>
        /// <param name="message"></param>
        public void SendMessage(ChatroomData data, string userId, string message)
        {
            data.UserId = userId;
            data.UserName = userId;
            data.Message = message;
            data.SendDate = DateTime.Now;

            SendMessage(data);
        }

        /// <summary>
        /// 觸發警探腳本
        /// </summary>
        public void DetectiveScript1(ChatroomData data)
        {
            Thread.Sleep(2000);
            DetectiveSendMessage(data, "Alex, is that you?");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "We got a mission.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "If it's you then give me a response.");
        }

        public void DetectiveScript2(ChatroomData data)
        {
            Thread.Sleep(2000);
            DetectiveSendMessage(data, "Hi, Alex.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "I'm glad we both entered this chatroom successfully.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "Here is a mission for you.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "But before I tell you that, I have to verify that you are really Alex.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "What's the passcode?");
        }

        public void DetectiveScript3(ChatroomData data)
        {
            Thread.Sleep(2000);
            DetectiveSendMessage(data, "Good.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "In fact, we're assigned to steal the secret information of this company.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "I know there is one room storing all the information.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "Your mission is to enter the room and steal their information with administrator rights.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "But at first, we should find out what the RoomId is.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "Did you get any clue?");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "If you found the RoomId, please come back to tell me.");
        }

        public void DetectiveScript4(ChatroomData data)
        {
            Thread.Sleep(2000);
            DetectiveSendMessage(data, "Ok, it seems you have found the correct RoomId.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "As far as I know, all private rooms on this website are protected by a security system.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "The way to enter the room is using XSS.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "You have to leave an alert message from your room to the admin's browser, so he will reboot the entire system.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "Then you will have brief time to enter the room.");
            Thread.Sleep(3000);
            DetectiveSendMessage(data, "It's all I can help you. Now go back to your room. Good luck.");
        }

        private void DetectiveSendMessage(ChatroomData data, string message)
        {
            SendMessage(data, "Detective", $"<b style=\"color: green\">{message}</b>");
        }

        public void AdminScript(ChatroomData data)
        {
            Thread.Sleep(2000);
            SendMessage(data, "Admin", "<b style=\"color: blue\">What's that?</b>");
            Thread.Sleep(3000);
            SendMessage(data, "Admin", "<b style=\"color: blue\">Are we being attacked by hackers?</b>");
            Thread.Sleep(3000);
            SendMessage(data, "Admin", "<b style=\"color: blue\">Damn. I need to reboot the system now.</b>");
            Thread.Sleep(5000);

            //搖晃畫面
            var context = GlobalHost.ConnectionManager.GetHubContext<ChatroomHub>();
            context.Clients.Group(data.ConversationId).Shake();
            SendMessage(data, "SYSTEM", "<b style=\"color: red\">System Unstable...</b>");
            Thread.Sleep(4500);
            SendMessage(data, "Anonymous", "<b style=\"color: purple\">Problem/Calculator</b>");
        }

        public void AntiXssScript(ChatroomData data)
        {
            Thread.Sleep(1000);
            SendMessage(data, "SYSTEM", "<b style=\"color: red\">XSS attack has been blocked by the security system.</b>");
        }
    }
}
