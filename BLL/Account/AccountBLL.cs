﻿using BLL.Common.Extensions;
using BLL.Problem;
using DAL.Account;
using DTO.Account;
using DTO.Problem;
using System;
using System.Collections.Generic;

namespace BLL.Account
{
    public class AccountBLL
    {
        private SolveRecordBLL SolveRecordBLL = new SolveRecordBLL();
        private AccountDAL AccountDAL = new AccountDAL();

        /// <summary>
        /// 取得所有已註冊的使用者資訊
        /// </summary>
        public IEnumerable<AccountData> GetAllUsers()
        {
            return AccountDAL.Select();
        }

        /// <summary>
        /// 檢查使用者帳戶是否存在
        /// </summary>
        /// <param name="form">表單資訊</param>
        public void CheckAccount(AccountData form)
        {
            if (!AccountDAL.HasUser(form))
            {
                throw new Exception("Incorrect UserId or Password");
            }
        }

        /// <summary>
        /// 檢查使用者帳戶是否存在
        /// </summary>
        /// <param name="form">表單資訊</param>
        public void CheckAccountWithSqlInjection(AccountData form)
        {
            if (!AccountDAL.HasUserWithSqlInjection(form))
            {
                throw new Exception("Incorrect UserId or Password");
            }
        }

        /// <summary>
        /// 檢查使用者帳戶是否存在 (難)
        /// </summary>
        /// <param name="form">表單資訊</param>
        public void CheckAccountWithSqlInjectionHard(AccountData form)
        {
            if (!AccountDAL.HasUserWithSqlInjectionHard(form))
            {
                throw new Exception("Incorrect UserId or Password");
            }
        }

        /// <summary>
        /// 檢查是否符合資格註冊
        /// </summary>
        /// <param name="form">表單資訊</param>
        /// <returns></returns>
        public void CheckSignup(AccountData form)
        {
            if (form.InvitationCode != "35920167")
            {
                throw new Exception("Invitation code is incorrect.");
            }
        }

        /// <summary>
        /// 執行註冊
        /// </summary>
        /// <param name="form">表單資訊</param>
        public void Signup(AccountData form)
        {
            //執行註冊
            AccountDAL.Insert(form);

            //完成問題
            SolveRecordBLL.SolveProblem(form.UserId, Level.Signup);
        }
    }
}
