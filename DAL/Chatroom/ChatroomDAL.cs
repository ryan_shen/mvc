﻿using DAL.Common;
using Dapper;
using Dapper.Contrib.Extensions;
using DTO.Chatroom;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DAL.Chatroom
{
    public class ChatroomDAL
    {
        public IEnumerable<ChatroomData> Select(ChatroomData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = "Select * From Chatroom Where 1 = 1";

                if (data.ConversationId != null)
                {
                    sql += " And ConversationId = @ConversationId ";
                }
                if (data.RoomId != null)
                {
                    sql += " And RoomId = @RoomId ";
                }
                if (data.UserId != null)
                {
                    sql += " And UserId = @UserId ";
                }
                if (data.Message != null)
                {
                    sql += " And Message like '%' + @Message + '%' ";
                }

                return connection.Query<ChatroomData>(sql, data);
            }
        }

        public void Insert(ChatroomData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                connection.Insert(data);
            }
        }
    }
}
