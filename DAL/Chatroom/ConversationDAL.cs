﻿using DAL.Common;
using Dapper;
using Dapper.Contrib.Extensions;
using DTO.Chatroom;
using DTO.Problem;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Chatroom
{
    public class ConversationDAL
    {
        public IEnumerable<ConversationData> SelectAll()
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = "Select * From Conversation Order By ProblemId";
                return connection.Query<ConversationData>(sql);
            }
        }

        public string GetConversationId(string roomId, string userId)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = @"Select ConversationId
                               From Conversation
                               Where RoomId = @RoomId and UserId = @UserId";
                return connection.Query<string>(sql, new { RoomId = roomId, UserId = userId }).FirstOrDefault();
            }
        }

        public void Insert(ConversationData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                connection.Insert(data);
            }
        }

        public void Update(ConversationData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                connection.Update(data);
            }
        }
    }
}
