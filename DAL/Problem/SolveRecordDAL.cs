﻿using DAL.Common;
using Dapper;
using Dapper.Contrib.Extensions;
using DTO.Problem;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Problem
{
    public class SolveRecordDAL
    {
        public IEnumerable<SolveRecordData> SelectAll()
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = "Select * From SolveRecord Order By ProblemId";
                return connection.Query<SolveRecordData>(sql);
            }
        }

        /// <summary>
        /// 取得使用者的問題清單
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<SolveRecordData> Select(string userId)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                //只秀出彩蛋關卡以前的關卡
                string sql = @"Select T1.ProblemId, T1.ProblemName, T2.UserId, T2.UserId, T2.SolveDate
                               From Problem T1
                               Left join SolveRecord T2 on T1.ProblemId = T2.ProblemId and T2.UserId = @UserId
                               Where T1.ProblemId <= 20
                               Order by T1.ProblemId";
                return connection.Query<SolveRecordData>(sql, new { UserId = userId });
            }
        }

        /// <summary>
        /// 取得使用者的單一問題紀錄
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="problemId"></param>
        /// <returns></returns>
        public SolveRecordData Select(string userId, int problemId)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = @"Select T1.ProblemId, T1.ProblemName, T2.UserId, T2.UserId, T2.SolveDate
                               From Problem T1
                               Inner join SolveRecord T2 on T1.ProblemId = T2.ProblemId
                               Where T2.UserId = @UserId 
                                 and T2.ProblemId = @ProblemId
                               Order by T1.ProblemId";
                return connection.Query<SolveRecordData>(sql, new { UserId = userId, ProblemId = problemId }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 取得關卡排名
        /// </summary>
        /// <param name="problemId"></param>
        /// <param name="rankCount">要取到第幾名</param>
        /// <returns></returns>
        public IEnumerable<SolveRecordData> GetRanking(int problemId, int rankCount)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = @"Select ROW_NUMBER() Over (Order by SolveDate) as Ranking,
                                      ProblemId, UserId, SolveDate
                               From SolveRecord 
                               Where ProblemId = @ProblemId
                               Order by SolveDate";
                return connection.Query<SolveRecordData>(sql, new { ProblemId = problemId }).Take(rankCount);
            }
        }

        public void Insert(SolveRecordData form)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                connection.Insert(form);
            }
        }

        public void Update(SolveRecordData form)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                connection.Update(form);
            }
        }
    }
}
