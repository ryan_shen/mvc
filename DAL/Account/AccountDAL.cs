﻿using DAL.Common;
using Dapper;
using Dapper.Contrib.Extensions;
using DTO.Account;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DAL.Account
{
    public class AccountDAL
    {
        public IEnumerable<AccountData> Select()
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = "Select * From Account";
                return connection.Query<AccountData>(sql);
            }
        }

        public bool HasUser(AccountData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = @"Select * From Account 
                               Where UserId = @UserId And Password = @Password";
                return connection.Query<AccountData>(sql, new
                {
                    data.UserId,
                    data.Password
                }).Any();
            }
        }

        public bool HasUserWithSqlInjection(AccountData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = $@"Select * From Account 
                                Where UserId = '{data.UserId}' And Password = '{data.Password}' And 1 <> 1";
                return connection.Query<AccountData>(sql).Any();
            }
        }

        public bool HasUserWithSqlInjectionHard(AccountData data)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                string sql = $@"Select * From Account 
                                Where (UserId = '{data.UserId}' And Password = '{data.Password}')
                                  And 1 <> 1";
                return connection.Query<AccountData>(sql).Any();
            }
        }

        public void Insert(AccountData form)
        {
            using (var connection = new SqlConnection(SqlHelper.GetConnectionString()))
            {
                connection.Insert(form);
            }
        }
    }
}
