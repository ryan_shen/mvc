﻿$(function () {
    $('[class$=form-link]').click(function (e) {
        $.when($('.form').fadeOut('fast'))
            .done(function () {
                $('#' + $(e.target).attr('for')).fadeIn('fast');
            });
    });
});