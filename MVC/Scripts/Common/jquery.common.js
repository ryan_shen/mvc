﻿//將 Scroll Bar 滾動到最底部
//offset: 捲到距離最底部差多少 px 的位置
$.fn.scrollBottom = function (offset) {
    //ScrollBar 區塊內的總高度 (包含被隱藏的)
    let scrollHeight = $(this).prop('scrollHeight');
    //ScrollBar 距離最高點的高度差
    let scrollTop = $(this).scrollTop();
    //ScrollBar 當前可視區域的高度 (= ScrollBar 所屬的 Container 高度)
    let scrollVisibleHeight = $(this).height();

    if (offset === undefined) {
        //回傳與最底部的距離
        return scrollHeight - scrollTop - scrollVisibleHeight;
    }
    //捲到距離最底部差 offset px 的位置
    $(this).scrollTop(scrollHeight - scrollVisibleHeight - offset);
};