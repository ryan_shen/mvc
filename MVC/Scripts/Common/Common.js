﻿$(function () {
    if (!sessionStorage.RoomId) {
        sessionStorage.RoomId = '071';
    }
})

function parseJsonDate(date) {
    date = date.replace('/Date(', '').replace(')/', '');
    return new Date(parseInt(date));
}