﻿$(function () {
    $('#ProblemList').toggleClass('flicker', sessionStorage.ProblemNotification === 'true');

    $('#ProblemList').click(function () {
        $(this).removeClass('flicker');
        sessionStorage.ProblemNotification = false;

        let $tempWindow = $('<div id="TempWindow">');
        $tempWindow.kendoWindow({
            action: ['close'],
            content: $('#ProblemList').attr('url'),
            draggable: false,
            height: '80vh',
            iframe: true,
            modal: true,
            resizable: false,
            title: 'Problem List',
            visible: false,
            width: '70vw',
            deactivate: function () {
                this.destroy();
            }
        }).data('kendoWindow').center().open();
    });

    $(window.self).keydown(function (e) {
        if (e.key === 'Escape') {
            let kendoWindow = window.parent.$('[data-role=window]');
            if (kendoWindow.length) {
                kendoWindow.data('kendoWindow').close();
                return false;
            }
        }
    });
});