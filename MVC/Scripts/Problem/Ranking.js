﻿$(function () {
    let grid = $('#Grid').kendoGrid({
        columns: [
            {
                title: 'Order',
                field: 'Ranking',
                width: '20%'
            },
            {
                title: 'UserId',
                field: 'UserId',
                width: '40%'
            },
            {
                title: 'SolveDate',
                field: 'SolveDate',
                width: '40%'
            }
        ],
        dataSource: {
            transport: {
                read: {
                    type: 'post',
                    dataType: 'json',
                    url: $('#GetRankingUrl').attr('url'),
                    data: {
                        problemId: parseInt($('#ProblemId').text())
                    }
                }
            },
        },
        height: '100vh',
        width: '100vw',
        dataBinding: function (data) {
            $.each(data.items, function (i, e) {
                if (e.SolveDate) {
                    e.SolveDate = parseJsonDate(e.SolveDate).toLocaleString('zh-TW', { hour12: false });
                }
            })
        }
    }).data('kendoGrid');

    $(window.self).keydown(function (e) {
        if (e.key === 'Escape') {
            let kendoWindow = window.parent.$('[data-role=window]');
            if (kendoWindow.length) {
                kendoWindow.data('kendoWindow').close();
                return false;
            }
        }
    });
});