﻿$(function () {
    let grid = $('#Grid').kendoGrid({
        columns: [
            {
                title: 'ProblemId',
                field: 'ProblemId',
                width: '15%'
            },
            {
                title: 'ProblemName',
                field: 'ProblemName',
                width: '35%'
            },
            {
                title: 'YourSolveDate',
                field: 'SolveDate',
                width: '35%'
            },
            {
                title: 'Ranking',
                attributes: {
                    style: 'text-align: center;'
                },
                command: [
                    {
                        name: 'Ranking',
                        text: 'Ranking',
                        click: function (e) {
                            var problemId = $(e.target).parent().siblings('[data-field=ProblemId]').text();
                            let $tempWindow = $('<div id="TempWindow">');
                            $tempWindow.kendoWindow({
                                action: ['close'],
                                content: $('#RankingUrl').attr('url') + '?problemId=' + problemId,
                                draggable: false,
                                height: '80vh',
                                iframe: true,
                                modal: true,
                                resizable: false,
                                title: 'Ranking List',
                                visible: false,
                                width: '70vw',
                                deactivate: function () {
                                    this.destroy();
                                }
                            }).data('kendoWindow').center().open();
                        }
                    },
                ],
                width: '15%'
            }
        ],
        dataSource: {
            transport: {
                read: {
                    type: 'post',
                    dataType: 'json',
                    url: $('#GetSelfRecordsUrl').attr('url')
                }
            },
        },
        height: '100vh',
        width: '100vw',
        dataBinding: function (data) {
            $.each(data.items, function (i, e) {
                if (e.SolveDate) {
                    e.SolveDate = parseJsonDate(e.SolveDate).toLocaleString('zh-TW', { hour12: false });
                }
            })
        }
    }).data('kendoGrid');

    $(window.self).keydown(function (e) {
        if (e.key === 'Escape') {
            let kendoWindow = window.parent.$('[data-role=window]');
            if (kendoWindow.length) {
                kendoWindow.data('kendoWindow').close();
                return false;
            }
        }
    });
});