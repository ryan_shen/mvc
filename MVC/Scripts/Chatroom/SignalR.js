﻿$(function () {
    $.connection.chatroomHub.on('sendMessage', function (data) {
        data.SendDate = new Date(data.SendDate).toLocaleTimeString();
        let template = kendo.template($('#MessageTemplate').html());
        $('#MessageContainer').append(template(data));
        $('#MessageContainer').scrollBottom(0);
    });

    $.connection.chatroomHub.on('shake', function (data) {
        $('body').addClass('shake-slow shake-constant');
        $.blockUI({
            message: '<h4>SYSTEM REBOOTING NOW</h4>',
            css: {
                color: 'red',
                border: '3px solid red'
            },
            overlayCSS: {
                backgroundColor: 'red'
            }
        });

        setTimeout(function () {
            $('body').removeClass('shake-slow shake-constant');
            $.unblockUI();
        }, 5000);
    });

    $.connection.chatroomHub.on('problemNotification', function () {
        $('#ProblemList').addClass('flicker');
        sessionStorage.ProblemNotification = true;
    });

    $.connection.hub.qs = { CId: $('#CId').text() };

    $.connection.chatroomHub.connection.start()
        .done(function () {
            console.log('SignalR connected');
        });

    $.connection.chatroomHub.connection.disconnected(function () {
        console.log('SignalR disconnected');
        setTimeout(function () {
            $.connection.chatroomHub.connection.start();
        }, 3000);
    });

    $.connection.chatroomHub.connection.reconnected(function () {
        console.log('SignalR reconnected');
    });
});