﻿$(function () {
    $.post($('#GetAllMessageUrl').attr('url'))
        .done(function (datas) {
            $.each(datas, function (i, e) {
                e.SendDate = parseJsonDate(e.SendDate).toLocaleTimeString();
                let template = kendo.template($('#MessageTemplate').html());
                $('#MessageContainer').append(template(e));
            });
            $('#MessageContainer').scrollBottom(0);
        });

    $('#ChatroomForm').submit(function () {
        if (!$('#Message').val()) {
            return false;
        }

        $.post(this.action, $(this).serialize())
            .done(function () {
                $.connection.chatroomHub.server.sendMessage($('#Message').val());
                $('#Message').val('');
            })
            .fail(function (data) {
                alert(data.responseText || 'Send message fail.');
            });
        return false;
    });

    $('body').keydown(function (e) {
        switch (e.key) {
            case 'Enter':
                if (e.shiftKey) {
                    $('#Message').val($('#Message').val() + '\n');
                    break;
                }
                $('#ChatroomForm').submit();
                break;
            default:
                return true;
        }
        return false;
    });
})