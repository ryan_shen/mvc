USE [MVC]
GO

/****** Object:  Table [dbo].[Account]    Script Date: 2020/10/26 上午 03:11:55 ******/
DROP TABLE [dbo].[Account]
GO

/****** Object:  Table [dbo].[Account]    Script Date: 2020/10/26 上午 03:11:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Account](
	[UserId] [nvarchar](20) NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Password] [varchar](4) NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



USE [MVC]
GO

/****** Object:  Table [dbo].[Chatroom]    Script Date: 2020/10/26 上午 02:29:34 ******/
DROP TABLE [dbo].[Chatroom]
GO

/****** Object:  Table [dbo].[Chatroom]    Script Date: 2020/10/26 上午 02:29:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Chatroom](
	[RoomId] [varchar](10) NOT NULL,
	[ConversationId] [varchar](50) NOT NULL,
	[UserId] [varchar](20) NOT NULL,
	[UserName] [nvarchar](20) NOT NULL,
	[Message] [nvarchar](1000) NOT NULL,
	[SendDate] [datetime] NOT NULL
) ON [PRIMARY]
GO


USE [MVC]
GO

/****** Object:  Table [dbo].[Conversation]    Script Date: 2020/10/26 上午 02:29:44 ******/
DROP TABLE [dbo].[Conversation]
GO

/****** Object:  Table [dbo].[Conversation]    Script Date: 2020/10/26 上午 02:29:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Conversation](
	[RoomId] [varchar](10) NOT NULL,
	[UserId] [varchar](20) NOT NULL,
	[ConversationId] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Conversation] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [MVC]
GO

/****** Object:  Table [dbo].[Problem]    Script Date: 2020/10/26 上午 02:32:56 ******/
DROP TABLE [dbo].[Problem]
GO

/****** Object:  Table [dbo].[Problem]    Script Date: 2020/10/26 上午 02:32:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Problem](
	[ProblemId] [int] NOT NULL,
	[ProblemName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Problem] PRIMARY KEY CLUSTERED 
(
	[ProblemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO





USE [MVC]
GO

/****** Object:  Table [dbo].[SolveRecord]    Script Date: 2020/10/26 上午 02:30:05 ******/
DROP TABLE [dbo].[SolveRecord]
GO

/****** Object:  Table [dbo].[SolveRecord]    Script Date: 2020/10/26 上午 02:30:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SolveRecord](
	[ProblemId] [int] NOT NULL,
	[UserId] [nvarchar](20) NOT NULL,
	[SolveDate] [datetime] NULL,
 CONSTRAINT [PK_SolveRecord] PRIMARY KEY CLUSTERED 
(
	[ProblemId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


