﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MVC.Models.Chatroom
{
    public class ChatroomForm
    {
        public string RoomId { get; set; }

        public string ConversationId { get; set; }

        public string UserId { get; set; }

        [Display(Name = "Name")]
        public string UserName { get; set; }

        [AllowHtml]
        public string Message { get; set; }

        public DateTime? SendDate { get; set; }
    }
}