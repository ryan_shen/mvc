﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC.Models.Attributes
{
    public class MvcAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// 覆寫進行身分驗證前的行為
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }

        /// <summary>
        /// 覆寫實際進行身分驗證時的行為
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //因為 MVC 不會自動從 Cookie 解析角色資訊，所以要手動解析之後加到 Context 裡面，才能提供 [Authorize] 的 Users、Roles 做判斷
            HttpCookie authCookie = httpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                //解密 .ASPXAUTH Cookie
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                //取得該使用者所有角色
                string[] roles = authTicket.UserData.Split(',');
                //把 Cookie 裡面存的角色資訊加回到 Context 裡面
                IIdentity id = new FormsIdentity(authTicket);
                IPrincipal principal = new GenericPrincipal(id, roles);
                httpContext.User = principal;
            }

            return base.AuthorizeCore(httpContext);
        }

        /// <summary>
        /// 覆寫沒有授權時的行為 (如果有掛 AllowAnonymous 就不會走進來了)
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var context = filterContext.HttpContext;

            //發 Post 因為沒辦法讓 Client 執行換頁的操作，所以改回傳錯誤訊息，不然會因為回傳 302 而進 done
            if (context.Request.IsAjaxRequest())
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Response.Write("操作失敗，系統已登出");
                context.Response.End();
            }

            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}