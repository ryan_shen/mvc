﻿using BLL.Problem;
using DTO.Problem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC.Models.Attributes
{
    public class ExceptionHandler : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var context = filterContext.HttpContext;

            //Calculator
            if (filterContext.Exception is DivideByZeroException)
            {
                new SolveRecordBLL().SolveProblem(context.Session["UserId"].ToString(), Level.Misconfiguration);
                throw new DivideByZeroException("", filterContext.Exception);
            }

            //不知為啥 throw Exception 到前端用 responseText 接都是 HTML，但我要的只有錯誤訊息
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            context.Response.Write(context.Session["UserId"] == null ? "Session expired, please login again." : filterContext.Exception.Message);
            context.Response.End();
        }
    }
}