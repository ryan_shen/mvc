﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models.Problem
{
    public class RegexForm
    {
        /// <summary>
        /// 第一題
        /// </summary>
        [Required]
        [RegularExpression(@"\d{4}-\d{6}")]
        public string Q1 { get; set; }

        /// <summary>
        /// 第二題
        /// </summary>
        [Required]
        [RegularExpression(@"(abc|cba)acb$")]
        public string Q2 { get; set; }

        /// <summary>
        /// 第三題
        /// </summary>
        [Required]
        [RegularExpression(@"^[abc][^abc]")]
        public string Q3 { get; set; }

        /// <summary>
        /// 第四題
        /// </summary>
        [Required]
        [RegularExpression(@"\w(\W)?(\d)*(\D(\s\S))")]
        public string Q4 { get; set; }

        /// <summary>
        /// 第五題
        /// </summary>
        [Required]
        [RegularExpression(@"..+\\.+\+.\.+\++")]
        public string Q5 { get; set; }
    }
}