﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models.Problem
{
    public class CustomPRNGForm
    {
        [Display(Name = "Your money")]
        public int Money { get; set; }

        [Display(Name = "Bets")]
        [Required]
        [RegularExpression("[0-9]+")]
        public int Bet { get; set; }

        [Display(Name = "Pips")]
        [Required]
        public int Pip { get; set; }
    }
}