﻿using MVC;
using MVC.Models;
using MVC.Models.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC.Models.Account
{
    public class LoginForm
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [Required]
        public string UserName { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [StringLength(4, MinimumLength = 4)]
        public string Password { get; set; }

        /// <summary>
        /// 邀請碼
        /// </summary>
        [Required]
        [StringLength(8, MinimumLength = 8)]
        public string InvitationCode { get; set; }
    }
}