﻿use MVC;

delete from Account
delete from Chatroom
delete from Conversation
delete from SolveRecord
	
delete from Problem
insert into Problem values (1, 'Signup')
insert into Problem values (2, 'MaskTheTitles')
insert into Problem values (3, 'CollapsedContent')
insert into Problem values (4, 'Decoder')
insert into Problem values (5, 'InfiniteAlerts')
insert into Problem values (6, 'Regex')
insert into Problem values (7, 'Background')
insert into Problem values (8, 'qolq')
insert into Problem values (9, 'PathTraversal')
insert into Problem values (10, 'Page20000')
insert into Problem values (11, 'DataInBrowser')
insert into Problem values (12, 'Unsubmittable')
insert into Problem values (13, 'FakeMessage')
insert into Problem values (14, 'PasscodeAndRoomId')
insert into Problem values (15, 'XSS')
insert into Problem values (16, 'Misconfiguration')
insert into Problem values (17, 'SqlInjection')
insert into Problem values (18, 'StackTrace')
insert into Problem values (19, 'PRNG')
insert into Problem values (20, 'Unauthorized')
insert into Problem values (21, 'SqlInjectionHard')