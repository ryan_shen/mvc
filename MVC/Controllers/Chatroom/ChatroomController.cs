﻿using BLL.Account;
using BLL.Chatroom;
using BLL.Common.Extensions;
using BLL.Problem;
using DTO.Account;
using DTO.Chatroom;
using DTO.Problem;
using DTO.User;
using MVC.Models.Account;
using MVC.Models.Chatroom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MVC.Controllers.Chatroom
{
    public class ChatroomController : MvcController
    {
        private readonly SolveRecordBLL SolveRecordBLL = new SolveRecordBLL();
        private readonly ConversationBLL ConversationBLL = new ConversationBLL();
        private readonly ChatroomBLL ChatroomBLL = new ChatroomBLL();
        private readonly AccountBLL AccountBLL = new AccountBLL();

        public ActionResult Chatroom(string roomId = "001")
        {
            ConversationId = ConversationBLL.GetConversationId(roomId, User.UserId);
            RoomId = roomId;

            ViewBag.ConversationId = ConversationId;
            ViewBag.RoomId = RoomId;

            if (roomId == "071")
            {
                SolveRecordBLL.SolveProblem(User.UserId, Level.DataInBrowser);
                return View("Unsubmittable");
            }
            else if (roomId == "405")
            {
                if (!SolveRecordBLL.IsSolved(User.UserId, Level.XSS))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
                if (!SolveRecordBLL.IsSolved(User.UserId, Level.SqlInjection))
                {
                    return View("SqlInjection");
                }
                if (!SolveRecordBLL.IsSolved(User.UserId, Level.StackTrace))
                {
                    StackTraceInputs = new List<string>();
                    ChatroomBLL.SendMessage(new ChatroomData
                    {
                        RoomId = "405",
                        ConversationId = ConversationId,
                        UserId = "SYSTEM",
                        UserName = "SYSTEM",
                        Message = $"<b style=\"color: red\">Please input 4 right numbers in separate messages to unlock the next stage.</b>",
                        SendDate = DateTime.Now
                    });
                }
            }
            else
            {
                //其餘房號都當成 001
                roomId = "001";
                ConversationId = ConversationBLL.GetConversationId(roomId, User.UserId);
                RoomId = roomId;
                ViewBag.ConversationId = ConversationId;
                ViewBag.RoomId = RoomId;
            }

            var form = new ChatroomForm
            {
                UserId = User.UserId,
                UserName = User.UserName
            };

            return View(form);
        }

        [HttpPost]
        public ActionResult GetAllMessage()
        {
            var chatroomData = ChatroomBLL.Select(new ChatroomData { ConversationId = ConversationId });

            if (RoomId == "071" && !SolveRecordBLL.IsSolved(User.UserId, Level.FakeMessage))
            {
                var scriptData = new ChatroomData
                {
                    RoomId = RoomId,
                    ConversationId = ConversationId
                };
                Task.Run(() => { ChatroomBLL.DetectiveScript1(scriptData); });
            }

            return Json(chatroomData);
        }

        [HttpPost]
        public ActionResult SendMessage(ChatroomForm form)
        {
            var chatroomData = form.ToType<ChatroomData>();

            chatroomData.RoomId = RoomId;
            chatroomData.ConversationId = ConversationId;
            chatroomData.UserId = User.UserId;
            chatroomData.SendDate = DateTime.Now;

            //XSS防護
            if (RoomId == "001"
                && Regex.IsMatch(chatroomData.Message, @".*<script>.*</script>.*", RegexOptions.IgnoreCase)
                && !SolveRecordBLL.IsSolved(User.UserId, Level.PasscodeAndRoomId))
            {
                chatroomData.Message = "";
                Task.Run(() => { ChatroomBLL.AntiXssScript(chatroomData); });
            }

            ChatroomBLL.SendMessage(chatroomData);

            //XSS 事件
            if (RoomId == "001"
                && Regex.IsMatch(chatroomData.Message, @".*<script.*>.*alert\(.*\).*</script>.*", RegexOptions.IgnoreCase)
                && SolveRecordBLL.IsSolved(User.UserId, Level.PasscodeAndRoomId))
            {
                SolveRecordBLL.SolveProblem(User.UserId, Level.XSS);
                Task.Run(() => { ChatroomBLL.AdminScript(chatroomData); });
            }

            //警探事件
            if (RoomId == "071" && chatroomData.UserName.ToLower() == "alex")
            {
                if (!SolveRecordBLL.IsSolved(User.UserId, Level.FakeMessage))
                {
                    SolveRecordBLL.SolveProblem(User.UserId, Level.FakeMessage);
                    Task.Run(() => { ChatroomBLL.DetectiveScript2(chatroomData); });
                }
                //Passcode
                else if (chatroomData.Message == "qwerty123" && SolveRecordBLL.IsSolved(User.UserId, Level.Regex))
                {
                    Task.Run(() => { ChatroomBLL.DetectiveScript3(chatroomData); });
                }
                //405房號
                else if (chatroomData.Message == "405" && SolveRecordBLL.IsSolved(User.UserId, Level.Page20000))
                {
                    SolveRecordBLL.SolveProblem(User.UserId, Level.PasscodeAndRoomId);
                    Task.Run(() => { ChatroomBLL.DetectiveScript4(chatroomData); });
                }
            }

            if (RoomId == "405")
            {
                if (!SolveRecordBLL.IsSolved(User.UserId, Level.StackTrace))
                {
                    StackTraceInputs.Add(form.Message);

                    if (StackTraceInputs.Count() >= 4)
                    {
                        var response = new ChatroomData
                        {
                            RoomId = "405",
                            ConversationId = ConversationId,
                            UserId = "SYSTEM",
                            UserName = "SYSTEM",
                            SendDate = DateTime.Now
                        };

                        if (StackTraceInputs.ElementAt(0) == "16" && StackTraceInputs.ElementAt(1) == "70"
                            && StackTraceInputs.ElementAt(2) == "84" && StackTraceInputs.ElementAt(3) == "85")
                        {
                            response.Message = $"<b style=\"color: red\">{Convert.ToBase64String(Encoding.Unicode.GetBytes("Ṗṙṍḃḷḕṁ╱ḈṳṣṭṑṃṔṚṆḠ "))}</b>";
                        }
                        else
                        {
                            response.Message = $"<b style=\"color: red\">Wrong inputs. Please try again.</b>";
                        }

                        ChatroomBLL.SendMessage(response);
                        StackTraceInputs.Clear();
                    }
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        /// <summary>
        /// 完成登入即通過 Unsubmittable，並導回到聊天室
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Unsubmittable(LoginForm form)
        {
            try
            {
                AccountBLL.CheckAccount(form.ToType<AccountData>());
            }
            catch (Exception e)
            {
                //回傳到前端的 ValidationSummary
                ModelState.AddModelError("", e.Message);
                return View(form);
            }

            SolveRecordBLL.SolveProblem(User.UserId, Level.Unsubmittable);

            ViewBag.ConversationId = ConversationId;
            ViewBag.RoomId = RoomId;

            return View("Chatroom", new ChatroomForm
            {
                UserId = User.UserId,
                UserName = User.UserName
            });
        }

        public ActionResult SqlInjection()
        {
            if (!SolveRecordBLL.IsSolved(User.UserId, Level.Unauthorized))
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            return View();
        }

        [HttpPost]
        public ActionResult SqlInjection(LoginForm form)
        {
            //檢查是否有非法字元，以免影響到我的資料庫
            var invalidKeywords = new string[] { "delete", "drop", "alter", "insert", "update", "schema", "column", "table" };
            foreach (var keyword in invalidKeywords)
            {
                if ((form.UserId?.ToLower().Contains(keyword) ?? false) 
                    || (form.Password?.ToLower().Contains(keyword) ?? false))
                {
                    throw new Exception("Invalid keyword detected.");
                }
            }

            //如果已經全破了就換超難彩蛋關卡
            if (SolveRecordBLL.IsSolved(User.UserId, Level.Unauthorized))
            {
                AccountBLL.CheckAccountWithSqlInjectionHard(form.ToType<AccountData>());
                SolveRecordBLL.SolveProblem(User.UserId, Level.SqlInjectionHard);
                return Content("You win! Thank you for playing.:)");
            }

            AccountBLL.CheckAccountWithSqlInjection(form.ToType<AccountData>());
            SolveRecordBLL.SolveProblem(User.UserId, Level.SqlInjection);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}