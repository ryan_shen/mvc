﻿using BLL.Common.Extensions;
using BLL.Problem;
using DTO.Problem;
using MVC.Models.Problem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC.Controllers.Problem
{
    [Authorize]
    public class ProblemController : MvcController
    {
        private readonly SolveRecordBLL SolveRecordBLL = new SolveRecordBLL();

        /// <summary>
        /// 顯示問題清單
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Problem()
        {
            return View();
        }

        /// <summary>
        /// 顯示解題排名
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Ranking(int problemId)
        {
            ViewBag.ProblemId = problemId;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetSelfRecords()
        {
            var userId = User.UserId.IfNullOrEmpty("");

            var allProblems = SolveRecordBLL.GetSelfRecords(userId);

            return Json(allProblems);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetRanking(int problemId)
        {
            var allProblems = SolveRecordBLL.GetRanking(problemId);

            return Json(allProblems);
        }

        /// <summary>
        /// 顯示問題清單的字元
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ShowTheTitles()
        {
            if (User.UserId == null)
            {
                return Content("Please login first.");
            }

            SolveRecordBLL.SolveProblem(User.UserId, Level.MaskTheTitles);

            return Content("All titles revealed.");
        }

        /// <summary>
        /// 解決漢堡下面的問題
        /// </summary>
        /// <returns></returns>
        public ActionResult Hiding()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.CollapsedContent);

            if (SolveRecordBLL.IsSolved(User.UserId, Level.PRNG))
            {
                ViewBag.EnableClickRobotEvent = true;
            }

            return View();
        }

        /// <summary>
        /// 解決 Footer 的問題
        /// </summary>
        /// <returns></returns>
        public ActionResult JsMakesYouAngry()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.Decoder);
            return View();
        }

        /// <summary>
        /// Regex 頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult MatchIt()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.InfiniteAlerts);
            return View();
        }

        /// <summary>
        /// 解決 Regex 的問題
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MatchIt(RegexForm form)
        {
            if (!ModelState.IsValid)
            {
                return Content("You lose. Please don't cheat.:)");
            }

            SolveRecordBLL.SolveProblem(User.UserId, Level.Regex);
            return Content("You win. The passcode is qwerty123");
        }

        /// <summary>
        /// qolq
        /// </summary>
        /// <returns></returns>
        public ActionResult ChromeOnly()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.Background);
            return View("qolq");
        }

        /// <summary>
        /// 完成 qolq
        /// </summary>
        /// <returns></returns>
        public ActionResult ItsBlob()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.qolq);

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket
            (
                version: 1,
                name: "Ryan",
                issueDate: DateTime.Now,
                expiration: DateTime.Now.AddDays(14),
                isPersistent: true,
                userData: "Admin"
            );
            var encryptedCookie = FormsAuthentication.Encrypt(ticket);
            ViewBag.AuthCookie = encryptedCookie;

            return View();
        }

        /// <summary>
        /// 目錄走訪攻擊
        /// </summary>
        /// <returns></returns>
        public ActionResult IHatePathTraversal()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.PathTraversal);
            return View();
        }

        /// <summary>
        /// Page20000 頁面
        /// </summary>
        /// <param name="page">第幾頁</param>
        /// <param name="pageSize">一頁幾筆資料</param>
        /// <param name="take">取幾筆資料 (= pageSize)</param>
        /// <param name="skip">跳過幾筆資料</param>
        /// <returns></returns>
        public ActionResult Grid(string page, string pageSize, string take, string skip)
        {
            if (page == null && pageSize == null && take == null && skip == null)
            {
                return RedirectToAction("Grid", new { take = "5", skip = "0", page = "1", pageSize = "5" });
            }

            //網址給的 page 是十六進位表示法，所以要得到 20000 要輸入 4E20
            ViewBag.Page = Convert.ToInt32(page, 16);
            return View();
        }

        /// <summary>
        /// 取得 Page20000 資料
        /// </summary>
        /// <param name="page">第幾頁</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetGridData(int page)
        {
            var gridDatas = new List<Page20000Data>();
            var random = new Random();
            var datasPerPage = 5;

            for (int i = (page - 1) * datasPerPage + 1; i <= page * datasPerPage; i++)
            {
                string data = "";
                string name = null;
                if (page == 20000 && i % 5 == 3)
                {
                    SolveRecordBLL.SolveProblem(User.UserId, Level.Page20000);
                    name = "RoomId";
                    data = "405";
                }

                gridDatas.Add(new Page20000Data
                {
                    Id = i,
                    Name = name ?? ((char)random.Next(65, 91)).ToString() + ((char)random.Next(97, 123)).ToString() + ((char)random.Next(97, 123)).ToString(),
                    Data = data,
                    Date = DateTime.Now
                });
            }

            return Json(new { GridDatas = gridDatas, Total = "500000" });
        }

        public ActionResult Calculator()
        {
            return View();
        }

        /// <summary>
        /// Misconfiguration
        /// </summary>
        /// <param name="operation"></param>
        /// <returns></returns>
        [HttpPost]
        public double Calculator(Operation operation)
        {
            if (!operation.N1.HasValue || !operation.N2.HasValue)
            {
                throw new Exception("Please follow the rules of the calculator.:)");
            }

            return DTO.Problem.Calculator.Calculate(operation.N1.Value, operation.Op, operation.N2.Value);
        }

        /// <summary>
        /// CustomPRNG 骰子遊戲
        /// </summary>
        /// <returns></returns>
        public ActionResult CustomPRNG()
        {
            if (!SolveRecordBLL.IsSolved(User.UserId, Level.Misconfiguration))
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            SolveRecordBLL.SolveProblem(User.UserId, Level.StackTrace);

            Money = 100;
            PipSetIndex = new Random().Next(0, 12);

            var model = new CustomPRNGForm
            {
                Money = Money,
                Bet = 10,
                Pip = 1
            };

            return View(model);
        }

        /// <summary>
        /// CustomPRNG 骰子遊戲
        /// </summary>
        /// <param name="form">表單</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CustomPRNG(CustomPRNGForm form)
        {
            var pipSet = new int[] { 4, 5, 6, 1, 3, 6, 2, 4, 1, 5, 3, 2 };
            int pip = pipSet[PipSetIndex];
            int bet = form.Bet;
            string message = null;

            if(bet <= 0)
            {
                throw new Exception("Bets must be above zero.");
            }
            if(Money < bet || Money == 0)
            {
                throw new Exception("Sorry, you don't have enough money.");
            }

            //依結果加減本金
            Money += (form.Pip == pip) ? bet : -bet;

            //指到下一個亂數
            PipSetIndex = (PipSetIndex + 1) % pipSet.Count();

            if (Money >= 2000)
            {
                SolveRecordBLL.SolveProblem(User.UserId, Level.PRNG);
                message = "You win! The ROBOT has been activated.";
            }

            return Json(new
            {
                Money = Money,
                Pip = pip,
                Message = message,
            });
        }

        public ActionResult Fina1Page()
        {
            if (!SolveRecordBLL.IsSolved(User.UserId, Level.PRNG))
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                //解密 .ASPXAUTH Cookie
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                //取得該使用者所有角色
                string[] roles = authTicket.UserData.Split(',');
                if (!roles.Any(i => i == "Admin"))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Administrator Only");
                }
            }

            return RedirectToAction("Congratulations");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Congratulations()
        {
            SolveRecordBLL.SolveProblem(User.UserId, Level.Unauthorized);

            return View();
        }
    }
}