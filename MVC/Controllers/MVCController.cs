﻿using DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class MvcController : Controller
    {
        public new User User { get; set; }

        //使用者現在所在房間 Guid
        public string ConversationId 
        {
            get { return Session["ConversationId"]?.ToString(); }
            set { Session["ConversationId"] = value; } 
        }

        /// <summary>
        /// 使用者當前所在的房間號
        /// </summary>
        public string RoomId
        {
            get { return Session["RoomId"]?.ToString(); }
            set { Session["RoomId"] = value; }
        }

        /// <summary>
        /// 在 StackTrace 關卡輸入的第幾組數字
        /// </summary>
        public List<string> StackTraceInputs
        {
            get { return Session["StackTraceInputs"] as List<string>; }
            set { Session["StackTraceInputs"] = value; }
        }

        public int Money
        {
            get { return Convert.ToInt32(Session["Money"]); }
            set { Session["Money"] = value; }
        }

        public int PipSetIndex
        {
            get { return Convert.ToInt32(Session["PipSetIndex"]); }
            set { Session["PipSetIndex"] = value; }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            User = new User
            {
                UserId = Session["UserId"]?.ToString(),
                UserName = Session["UserName"]?.ToString()
            };
        }
    }
}