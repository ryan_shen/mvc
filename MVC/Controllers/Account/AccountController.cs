﻿using BLL.Account;
using BLL.Common.Extensions;
using DTO.Account;
using MVC.Models.Account;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Security;

namespace MVC.Controllers.Account
{
    [AllowAnonymous]
    public class AccountController : MvcController
    {
        private AccountBLL AccountBLL = new AccountBLL();

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            //清除 SessionId
            Session.Clear();
            //清除 .ASPXAUTH
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return new HttpStatusCodeResult(HttpStatusCode.Redirect);
        }

        [HttpPost]
        public ActionResult Login(LoginForm form)
        {
            try
            {
                AccountBLL.CheckAccount(form.ToType<AccountData>());
            }
            catch (Exception ex)
            {
                //回傳到前端的 ValidationSummary
                ModelState.AddModelError("", ex.Message);
                return View(form);
            }

            var user = AccountBLL.GetAllUsers().FirstOrDefault(i => i.UserId == form.UserId);
            Session["UserId"] = user.UserId;
            Session["UserName"] = user.UserName;

            FormsAuthentication.RedirectFromLoginPage(user.UserId, false);
            return new HttpStatusCodeResult(HttpStatusCode.Redirect);
        }

        [HttpPost]
        public ActionResult Signup(LoginForm form)
        {
            //檢核註冊
            AccountBLL.CheckSignup(form.ToType<AccountData>());

            //執行註冊
            AccountBLL.Signup(form.ToType<AccountData>());

            return Login(form);
        }
    }
}