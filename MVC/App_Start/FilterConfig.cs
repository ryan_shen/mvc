﻿using MVC.Models.Attributes;
using System.Web;
using System.Web.Mvc;

namespace MVC
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            //預設讓全網站都需要權限
            filters.Add(new MvcAuthorizeAttribute());

            //錯誤處理
            filters.Add(new ExceptionHandler());
        }
    }
}
