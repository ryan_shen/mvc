﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "PathTraversal",
                url: "Account/",
                defaults: new { controller = "Problem", action = "IHatePathTraversal" }
            );

            routes.MapRoute(
                name: "Chatroom",
                url: "Chatroom/",
                defaults: new { controller = "Chatroom", action = "Chatroom", roomId = "001" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
        }
    }
}
