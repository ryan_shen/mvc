﻿using System.Web.Optimization;

namespace MVC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/ThirdParty/jquery-{version}.min.js",
                        "~/Scripts/ThirdParty/jquery.validate.min.js",
                        "~/Scripts/ThirdParty/jquery.validate.unobtrusive.min.js",
                        "~/Scripts/ThirdParty/jquery.blockUI.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/ThirdParty/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/ThirdParty/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo")
                .Include("~/Scripts/ThirdParty/kendo.all.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                "~/Scripts/Shared/Layout.js",
                "~/Scripts/Common/Common.js",
                "~/Scripts/Common/jquery.common.js"));
        }
    }
}
